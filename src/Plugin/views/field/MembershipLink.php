<?php
 
/**
 * @file
 * Definition of Drupal\group_field_membership\Plugin\views\field\MembershipLink
 */
 
namespace Drupal\group_field_membership\Plugin\views\field;
 
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
 
/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("membership_link")
 */
class MembershipLink extends FieldPluginBase
{

    /**
     * {@inheritdoc}
     */
    public function usesGroupBy()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function query()
    {
			// No query
    }
 
    /**
     * {@inheritdoc}
     */
    protected function defineOptions()
    {
        $options = parent::defineOptions();
 
        return $options;
    }
 
    /**
     * {@inheritdoc}
     */
    public function buildOptionsForm(&$form, FormStateInterface $form_state)
    {
        parent::buildOptionsForm($form, $form_state);
    }
 
    /**
     * {@inheritdoc}
     */
    public function render(ResultRow $values)
    {
        $link = null;

				// Get group entity
        if (isset($values->_entity) && $entity = $values->_entity) {
            $group_id = $entity->Id();

						// Get membership link for group 
            $link = group_field_membership_link($group_id);
        }
        return $link;
    }
}
