# Group Field Membership Documentation

## About
This module adds a pseudo-field to group entities and group views, providing a link to join or leave the group depending upon access permissions.

## Installation
Install as you would normally install a contributed Drupal module. Visit *https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules*
for further information.

## Usage
After installation the new field will be available for display in group entities and views.
